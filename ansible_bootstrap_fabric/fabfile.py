from fabric.api import env, sudo

env.hosts = ["52.198.223.132",
             "52.199.175.235",
             "52.192.79.122",
             "54.65.127.22",
             "13.112.91.25",
             "13.113.53.42",
             "54.64.230.97",
             "54.178.194.34",
             "13.113.92.156"
             ]

def init_ansible():
    sudo('apt-get update')
    sudo('apt-get -y install python aptitude')
